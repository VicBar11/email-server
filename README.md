# Kick Off 1 "email-server"

## Introducción

Esta tarea consiste en aplicar el concepto del protocolo SMTP visto en clase, la idea es poder implementar un servidor utilizando este protocolo. El objetivo de este servidor es tener todos los correos electrónicos físicamente en el lugar que nosotros designemos, además de implementar el servidor, debe ser capaz de enviar y recibir mensajes, incluso recibiendo archivos adjuntos utilizando MIME.

Además de esto se espera que el programa permita a un usuario autenticarse y a partir de esto poder descargar su buzón de entrada y visualizar los mensajes de algún cliente de correo, todo esto utilizando IMAP y un servicio de notificaciones NNTP.

Por último, a partir de un csv yo podría poner los destinatarios de cierto correo, para que el sistema se lo envíe a todos (ejemplo de la Asamblea Legislativa), haciendo todo desde la terminal.

## Ambiente de Desarrollo

Se utilizará PyCharm para el desarrollo de la herramienta, como se menciona en la documentación se utilizará la biblioteca Twisted para que la implementación sea más sencilla, por último todo se hará sobre el kernel Linux, utilizando la distro Ubuntu.





